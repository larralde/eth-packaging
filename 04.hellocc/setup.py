import os
import setuptools

extensions = [
    setuptools.Extension(
        "hellocc.hello",
        [os.path.join("hellocc", "hello.c")],
    )
]

setuptools.setup(
    ext_modules=extensions,
)
