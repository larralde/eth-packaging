#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

// --------------------------------------------------------------------------

PyDoc_STRVAR(
  hello_hello___doc__,
  "hello()\n"
  "--\n"
  "\n"
  "Say hello.\n"
  "\n"
);

static PyObject*
hello_hello(PyObject* self, PyObject *args)
{
    printf("Oh hey I'm a C program! Hello there!\n");
    return Py_None;
}

// --- cursor module ---------------------------------------------------------

static struct PyMethodDef hellomodule_methods[] = {
    {"hello", hello_hello, METH_VARARGS, hello_hello___doc__},
    {NULL, NULL, 0, NULL}  /* sentinel */
};

static struct PyModuleDef PyHello_Module = {
    PyModuleDef_HEAD_INIT,
    .m_name     = "hello",
    .m_doc      = NULL,
    .m_size     = 0,
    .m_methods  = hellomodule_methods,
    .m_traverse = NULL,
    .m_clear    = NULL,
    .m_free     = NULL,
};

PyMODINIT_FUNC
PyInit_hello(void)
{
    PyObject* m           = NULL;

    /* Create the module and initialize state */
    m = PyModule_Create(&PyHello_Module);
    if (m == NULL)
        goto exit;

exit:
    return m;

fail:
    Py_DECREF(m);
    return NULL;
}

