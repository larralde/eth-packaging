# coding: utf-8
from .hello import hello

__author__ = "Martin Larralde <martin.larralde@embl.de>"
__version__ = "0.1.0"
__license__ = "MIT"
__all__ = ["hello"]

