import pickle
import pathlib

import numpy


class LinearModel:

    @classmethod
    def load(self):
        f = pathlib.Path(__file__).absolute().parent
        with f.joinpath("model.pkl").open("rb") as f:
            return pickle.load(f)

    def __init__(self, coef, intercept):
        self.coef = coef
        self.intercept = intercept

    def __call__(self, X):
        return X @ self.coef + self.intercept


def predict(X):
    lm = LinearModel.load()
    Y = lm(X)
    return Y > 0.5

